# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

class LinkedListNode:
    def __init__(self, value=None, link=None):
        self.value = value
        self.link = link
    
class LinkedList:
    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail
        self._length = 0 #will need to be updated when new nodes are added or removed
    
    #Length instance attribute (read-only)
    @property
    def length(self):
        return self._length
    
    #Helper method for traversing the list
    def traverse(self, idx):
        if (self._length == 0 or idx > (self._length - 1)): #if empty, idx out of bounds
            raise IndexError("idx out of range")
        if not isinstance(idx, int): #if idx is not an integer
            raise TypeError("idx must be an integer")
        i = 0
        _current_node = self.head
        while i < idx:
            _current_node = _current_node.link
            i += 1
        return _current_node
    
    #List can be searched
    def get(self, idx):
        if idx < 0:
            idx = self._length + idx
        node = self.traverse(idx)
        return node.value

    #Values can be inserted at any index
    def insert(self, value, idx=None):
        _new_node = LinkedListNode(value)
        if idx is None:  #idx None (default)
            idx = self._length
        if self.head is None: #empty list, insert first node
            self.head = _new_node
            self.tail = _new_node
            self._length += 1
            return None
        elif (idx == self._length): #insert at tail (default)
            self.tail.link = _new_node
            self.tail = _new_node
            self._length += 1
            return None
        elif idx == 0: #insert before head
            _new_node.link = self.head
            self.head = _new_node
            self._length += 1
            return None
        else: #insert between two nodes
            this_idx_node = self.traverse(idx - 1)
            next_idx_node = this_idx_node.link
            _new_node.link = next_idx_node
            this_idx_node.link = _new_node
            self._length += 1
            return None
    
    #Values can be removed from any location
    def remove(self, idx=0):
        if (self._length == 0 or idx>(self._length - 1)): #if list empty, idx out of bounds
            raise IndexError("idx out of range")
        if not isinstance(idx, int): #if idx is not an integer
            raise TypeError("idx must be an integer")
        if idx == 0: #remove head (default)
            _val = self.head.value
            self.head = self.head.link
            self._length -= 1
            if self._length == 0:
                self.tail = None
            return _val
        previous_idx_node = self.traverse(idx - 1)
        this_idx_node = previous_idx_node.link
        _val = this_idx_node.value
        next_idx_node = this_idx_node.link
        previous_idx_node.link = next_idx_node
        if next_idx_node is None:
            self.tail = previous_idx_node
        self._length -= 1
        return _val

