# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
# 
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.

class LinkedQueueNode:
    def __init__(self, value, link=None):
        self.value = value
        self.link = link

class LinkedQueue:
    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail

    #enqueue: add node to tail
    def enqueue(self, val):
        n = LinkedQueueNode(val) #instantiate new node with value
        if self.head is None: #if queue is empty
            self.head = n #head and tail both point to the same node
            self.tail = n
        else:
            self.tail.link = n #link for tail becomes new node
            self.tail = n #tail points to new tail
        return None

    #dequeue: remove node from the head
    def dequeue(self):
        assert self.head, "Queue is empty." #queue cannot be empty
        _value = self.head.value #store value for node at head
        self.head = self.head.link #assign head property to node it points to
        if self.head is None: #if final item removed, tail is none
            self.tail = None
        return _value